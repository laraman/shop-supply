function onRequest(request, response)
{
	nlapiLogExecution('error', 'onRequest', '');
	var recordType = request.getParameter('recordType');
	var max_column_id = request.getParameter('max_column_id');
	var min_column_id = request.getParameter('min_column_id');

	var searchColumn = [];
    searchColumn.push(new nlobjSearchColumn('name'));
    searchColumn.push(new nlobjSearchColumn(max_column_id));
    searchColumn.push(new nlobjSearchColumn(min_column_id));
    var searchResults = nlapiSearchRecord(recordType, null, null, searchColumn);
    var listArray = new Array();    
    for (i in searchResults) {    	    	
        listArray[searchResults[i].id] = {
        	name: searchResults[i].getValue('name'),
        	min: searchResults[i].getValue(min_column_id),
        	max: searchResults[i].getValue(max_column_id)
        };                
    }

    nlapiLogExecution('error', 'result', JSON.stringify(listArray));
    response.setContentType('JSON');
	response.write(JSON.stringify(listArray));		
}