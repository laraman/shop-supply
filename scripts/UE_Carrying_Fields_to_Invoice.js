function beforeLoad(type,form) {
	nlapiLogExecution('DEBUG', 'type', type);
 	try {
 		var currentContext = nlapiGetContext();
		nlapiLogExecution('DEBUG', 'context', currentContext.getExecutionContext());

		if (type != 'create') return;

		var rec = nlapiGetNewRecord();		

		var refRecId = nlapiGetFieldValue('createdfrom');

		if(!refRecId) return;


		var itemFulfillmentRec;
		var searchResult=nlapiSearchRecord
				(
					'itemfulfillment',
					null,
					[
						['createdfrom','is',refRecId]
					],
					new nlobjSearchColumn('internalid')
				);

		nlapiLogExecution('DEBUG', 'result', JSON.stringify(searchResult));
		if (searchResult && searchResult.length > 0 ) {
			nlapiLogExecution('DEBUG', 'fulfillment id', searchResult[0].getId());	
			itemFulfillmentRec = nlapiLoadRecord('itemfulfillment', searchResult[0].getId());		
		}

		
		nlapiLogExecution('DEBUG', 'itemFulfillmentRec', JSON.stringify(itemFulfillmentRec));

		var lineFieldsToAdd = [			
			'custcol_custom_serial_num'
		];

		if(itemFulfillmentRec) {
			for (var i = 1; i <= rec.getLineItemCount('item'); i++) {			
				rec.selectLineItem('item', i);				
				nlapiLogExecution('DEBUG', 'line num', i);
				for (var k = 0; k < lineFieldsToAdd.length; k++) {								
					var field_id = lineFieldsToAdd[k];
					var field_val = itemFulfillmentRec.getLineItemValue('item', field_id, i);
					if(field_val) {
						nlapiLogExecution('DEBUG', field_id, field_val);
						rec.setCurrentLineItemValue('item', field_id, field_val);	
					}
				}
				rec.commitLineItem('item');
			}	

			if(itemFulfillmentRec.getFieldValue('custbody2')) {
				rec.setFieldValue('custbody2', itemFulfillmentRec.getFieldValue('custbody2'));		
			}

		}
	}
	catch (e) {
		nlapiLogExecution('ERROR', e.getCode(), e.getDetails());
	}
}

function afterSubmit(type,form)
{
	
	try
	{
		var currentContext = nlapiGetContext();
		nlapiLogExecution('DEBUG', 'context', currentContext.getExecutionContext());
		nlapiLogExecution('DEBUG', 'type', type);

		if (type != 'create' || currentContext.getExecutionContext() == 'userinterface') return;

		var rec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());		

		var refRecId = nlapiGetFieldValue('createdfrom');

		if(!refRecId) return;


		var itemFulfillmentRec;
		var searchResult=nlapiSearchRecord
				(
					'itemfulfillment',
					null,
					[
						['createdfrom','is',refRecId]
					],
					new nlobjSearchColumn('internalid')
				);

		nlapiLogExecution('DEBUG', 'result', JSON.stringify(searchResult));
		if (searchResult && searchResult.length > 0 ) {
			nlapiLogExecution('DEBUG', 'fulfillment id', searchResult[0].getId());	
			itemFulfillmentRec = nlapiLoadRecord('itemfulfillment', searchResult[0].getId());		
		}

		
		nlapiLogExecution('DEBUG', 'itemFulfillmentRec', JSON.stringify(itemFulfillmentRec));

		var lineFieldsToAdd = [			
			'custcol_custom_serial_num'
		];

		if(itemFulfillmentRec) {
			for (var i = 1; i <= rec.getLineItemCount('item'); i++) {			
				rec.selectLineItem('item', i);				
				nlapiLogExecution('DEBUG', 'line num', i);
				for (var k = 0; k < lineFieldsToAdd.length; k++) {								
					var field_id = lineFieldsToAdd[k];
					var field_val = itemFulfillmentRec.getLineItemValue('item', field_id, i);
					if(field_val) {
						nlapiLogExecution('DEBUG', field_id, field_val);
						rec.setCurrentLineItemValue('item', field_id, field_val);	
					}
				}
				rec.commitLineItem('item');
			}	

			if(itemFulfillmentRec.getFieldValue('custbody2')) {
				rec.setFieldValue('custbody2', itemFulfillmentRec.getFieldValue('custbody2'));		
			}

			nlapiSubmitRecord(rec, true, true);
		}
		// createdfrom
	
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', e.getCode(), e.getDetails());
	}
}


function isNullOrUndefined(value) {
    if (value === null) {
        return true;
    }
    if (value === undefined) {
        return true;
    }
    return false;
}

function isEmpty(stValue) {
    if (isNullOrUndefined(stValue) === true) {
        return true;
    }
    if (stValue.length == 0) {
        return true;
    }
    return false;
}