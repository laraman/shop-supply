var cur_linenum;
var cur_item;
// var cur_wire_range_rec;
var actualMaterialList = [];	// get Actual Material List
var wireDiaRangeList = [];
var casingSizeList = [];
var PCDGrainList = [];
var PCDHoleRangeList = [];
function myPageInit(type) {
 	console.log ('--myPageInit--' );
 	console.log ('type=' + type); 
 	// Active Material List Popup
 	jQuery('#custcol_act_materal_popup_list').on('click', function() {
 		console.log('Open Material List Popup!'); 		
 		waitMaterialListPopup(); 		
 	});

 	

 	casingSizeList = getCustomList('customlist200');
 	actualMaterialList = getCustomList('customlist207');
 	wireDiaRangeList = getCustomRecordList('customrecord_incoming_wire_dia_range', 'custrecord_min', 'custrecord_max');	
 	PCDGrainList = getCustomList('customlist210');
 	PCDHoleRangeList = getCustomRecordList('customrecord_pcd_hole_size', 'custrecord_pcd_hole_min', 'custrecord_pcd_hole_max');	
}

function waitMaterialListPopup() {
	if(jQuery('#popup_outerdiv[fieldname="custcol_act_materal"] #inner_popup_div').length == 0) {
		setTimeout(function() {waitMaterialListPopup();}, 500);
	} else {
		console.log('Material List Popup Opened!');
		setMaterialList();
	}
}

function setPCDGrainList() {
	console.log('-- Set PCD Grain List --');
	jQuery('div.uir-tooltip-content div.dropdownDiv > div').show();

	var line_index = nlapiGetCurrentLineItemIndex('item');
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	
	if(!isEmpty(item_id)) {			
		if(!cur_item || cur_item['id'] != item_id) {
			cur_item = getLineItemRecord(item_id);
		}
		var dropdown_list = cur_item['custitem_pcd_grain'];
		console.log('Item Dropdown List', dropdown_list);
		if(!isNullOrUndefined(dropdown_list)) {
			if(PCDGrainList.length == 0) {
				PCDGrainList = getCustomList('customlist210');				
			}

			var dropdown_name_list = [];
			for(var i = 0; i < dropdown_list.length; i++) {
				dropdown_name_list.push(PCDGrainList[dropdown_list[i]]);
			}

			console.log('Loop All Drop down Option');
			jQuery('div.uir-tooltip-content div.dropdownDiv > div').each(function() {
				var _name = jQuery(this).text();				
				console.log(_name);									
				if(dropdown_name_list.indexOf(_name) == -1) {
					jQuery(this).hide();
				}
			})			
		}		
	}
}


function setMaterialList() {
	console.log('-- Set Actual Material List --');
	var line_index = nlapiGetCurrentLineItemIndex('item');
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	
	if(!isEmpty(item_id)) {		
		
		// console.log('All materialList', materialList);
		if(!cur_item || cur_item['id'] != item_id) {
			cur_item = getLineItemRecord(item_id);
		}
		var dropdown_list = cur_item['custitem9'];
		console.log('Item Dropdown List', dropdown_list);
		if(!isNullOrUndefined(dropdown_list)) {
			console.log('Loop All Drop down Option');
			jQuery('#popup_outerdiv[fieldname="custcol_act_materal"] table#tbl_Search').parent().closest('table').css('visibility', 'hidden');	// hide Search Box

			jQuery('#popup_outerdiv[fieldname="custcol_act_materal"] #inner_popup_div tr').each(function() {
				var _id = jQuery(this).attr('uir-multiselect-id');				
				console.log(_id);
				if(!isEmpty(_id)) {					
					if(dropdown_list.indexOf(_id) == -1) {
						jQuery(this).hide();
					}
				}				
			})			
		}		
	}
}

function setWireDiameterRange() {
	console.log('-- Set Wire Diameter Range --');	
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	if(!isEmpty(item_id)) {		
		if(!cur_item || cur_item['id'] != item_id) {
			cur_item = getLineItemRecord(item_id);
		}
		
		var fieldValue = cur_item['custitem_incoming_wire_dia_range'];		
		console.log('Diameter Range', fieldValue);
		if(!isEmpty(fieldValue)) {
			if(wireDiaRangeList.length == 0) {
				wireDiaRangeList = getCustomRecordList('customrecord_incoming_wire_dia_range', 'custrecord_min', 'custrecord_max');					
			}			
			var fieldText;
			if(wireDiaRangeList[fieldValue]) {				
			 	fieldText= wireDiaRangeList[fieldValue].name;
			}
			if(!isEmpty(fieldText)) {
				jQuery('input#custcol_inc_wire_diameter_formattedValue').attr('placeholder', fieldText);
				jQuery('input#custcol_inc_wire_diameter_formattedValue').attr('title', fieldText);
			}
		} else {
			jQuery('input#custcol_inc_wire_diameter_formattedValue').attr('placeholder', 'No value range defined.');
			jQuery('input#custcol_inc_wire_diameter_formattedValue').attr('title', 'No value range defined.');
		}
	}
}

function setPCDHoleRange() {
	console.log('-- Set PCD Hole Range --');	
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	if(!isEmpty(item_id)) {		
		if(!cur_item || cur_item['id'] != item_id) {
			cur_item = getLineItemRecord(item_id);
		}
		
		var fieldValue = cur_item['custitem_pcd_hole_size'];		
		console.log('PCD Hole Range', fieldValue);
		if(!isEmpty(fieldValue)) {
			if(PCDHoleRangeList.length == 0) {
				PCDHoleRangeList = getCustomRecordList('customrecord_pcd_hole_size', 'custrecord_pcd_hole_min', 'custrecord_pcd_hole_max');				
			}			
			var fieldText;
			if(wireDiaRangeList[fieldValue]) {				
			 	fieldText= PCDHoleRangeList[fieldValue].name;
			}
			if(!isEmpty(fieldText)) {
				jQuery('input#custcol_pcd_hole_size_formattedValue').attr('placeholder', fieldText);
				jQuery('input#custcol_pcd_hole_size_formattedValue').attr('title', fieldText);
			}
		} else {
			jQuery('input#custcol_pcd_hole_size_formattedValue').attr('placeholder', 'No value range defined.');
			jQuery('input#custcol_pcd_hole_size_formattedValue').attr('title', 'No value range defined.');
		}
	}
}

function getLineItemRecord(item_id) {
	console.log('load item', item_id);
	var url = nlapiResolveURL('SUITELET', 'customscript_bb_get_item_record', 'customdeploy_bb_get_item_record');
	var jsonData = {item_id: item_id}
	var response = nlapiRequestURL(url, jsonData, null, 'POST');	
	var item = JSON.parse(response.getBody());    
    console.log('ITEM: ', item);
	return item;
}

function getCustomList(listScriptId) {
	console.log('--getCustomList--', listScriptId);
	var url = nlapiResolveURL('SUITELET', 'customscript_bb_get_custom_list', 'customdeploy_bb_get_custom_list');
	var jsonData = {listid: listScriptId}
	var response = nlapiRequestURL(url, jsonData, null, 'POST');	
	var listArray = JSON.parse(response.getBody());    
    console.log('customList: ' + listScriptId, listArray);
    return listArray;
}

function getCustomRecordList(recordType, min_column_id, max_column_id) {
	console.log('--getCustomRecordList--', recordType);
	var url = nlapiResolveURL('SUITELET', 'customscript_bb_get_custom_record_list', 'customdeploy_bb_get_custom_record_list');
	var jsonData = {
		recordType: recordType,
		min_column_id: min_column_id,
		max_column_id: max_column_id
	}
	var response = nlapiRequestURL(url, jsonData, null, 'POST');	
	var listArray = JSON.parse(response.getBody()); 
    console.log('customRecordList: ' + recordType, listArray);
    return listArray;
}



function myValidateField(type, name, linenum) {	
	console.log ('--myValidateField--', name);
	// console.log ('type=' + type);
	// console.log ('name=' + name );
	// console.log ('linenum=' + linenum);	
	var disabledValidation = nlapiGetFieldValue('custbody_disable_custom_validation');	
	if(type == 'item' && disabledValidation != 'T') {
		if(name == 'custcol_act_materal') {
			return dropdownValidation(name, linenum, 'custitem9', actualMaterialList);			
		}
		if(name == 'custcol_inc_wire_diameter') {			
			return rangeValidation(name, linenum, 'custitem_incoming_wire_dia_range', wireDiaRangeList); 
		}

		if(nlapiGetCurrentLineItemValue('item', 'class') == '2' || nlapiGetCurrentLineItemValue('item', 'class') == '7')  {		// when Item Class is PCD Dies	or	PCD Die Recutting	
			if(name == 'custcol_pcd_grain') {
				return dropdownValidation(name, linenum, 'custitem_pcd_grain', PCDGrainList);
			}
		}

		if (name == 'custcol_pcd_hole_size') {
			return rangeValidation(name, linenum, 'custitem_pcd_hole_size', PCDHoleRangeList); 
		}
	}
	return true;
}

function truncateDecimalPart(field_name, linenum, digits) {	
	var fieldValue = nlapiGetLineItemValue('item', field_name, linenum);
	console.log('truncateDecimalPart', fieldValue);
	if(!isEmpty(fieldValue) && !isNaN(fieldValue)) {		
		fieldValue = parseFloat(fieldValue).toFixed(2);		
		nlapiSetCurrentLineItemValue('item', field_name, fieldValue);	
	}
}

function dropdownValidation(field_name, linenum, item_field_id, customList) {
	console.log('--Dropdown Field Validation--');
	var fieldValue = nlapiGetCurrentLineItemValue('item', field_name);
	console.log('current field value', fieldValue);
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	if(!isEmpty(item_id) && !isEmpty(fieldValue)) {						
		if(!cur_item || cur_item['id'] != item_id) {			
			cur_item = getLineItemRecord(item_id);
		}						
		var itemFieldValue = cur_item[item_field_id];
		console.log('item dropdown list', itemFieldValue);

		if(!isEmpty(itemFieldValue) && itemFieldValue.indexOf(fieldValue) == -1) {
			var itemFieldTextList = [];
			for(var i = 0; i < itemFieldValue.length; i++) {
				var fieldText = customList[itemFieldValue[i]];
				if(!isEmpty(fieldText)) {
					itemFieldTextList.push(fieldText);	
				}				
			}
			if(itemFieldTextList.length > 0) {
				itemFieldTextList = itemFieldTextList.join(',\n');
				alert('This field should be one of following:\n' + itemFieldTextList);
			}			
			return false;
		}

	}
	return true;
}

function rangeValidation(field_name, linenum, item_field_id, customRecordList) {
	console.log('--Range Field Validation--');
	var fieldValue = nlapiGetCurrentLineItemValue('item', field_name);		
	console.log('current field value', fieldValue);
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	if(!isEmpty(item_id) && !isEmpty(fieldValue) && !isNaN(fieldValue)) {		
		if(!cur_item || cur_item['id'] != item_id) {
			cur_item = getLineItemRecord(item_id);
		}		
		var itemFieldValue = cur_item[item_field_id];

		if(!isEmpty(itemFieldValue)) {			
			var rangeRecord = customRecordList[itemFieldValue];
			console.log('Custom Range Record', rangeRecord);
			if(rangeRecord) {
				console.log('Test 1');
				if(!isNaN(fieldValue) && !isEmpty(rangeRecord.min) && !isNaN(rangeRecord.max) && !isEmpty(rangeRecord.max) && !isNaN(rangeRecord.min)) {
					if(parseFloat(fieldValue) < parseFloat(rangeRecord.min) || parseFloat(fieldValue) > parseFloat(rangeRecord.max)) {
						console.log('Test 2');
						alert('This field should be in ' + rangeRecord.name);
						return false;
					}
				}				
			}
			
		}

	}
	return true;	
}






function togglePCD_Fields() {
	console.log('--Enable/Disable PCD Fields--');
	var fieldValue = nlapiGetCurrentLineItemValue('item', 'class');	
	if(nlapiGetCurrentLineItemValue('item', 'class') == '2' || nlapiGetCurrentLineItemValue('item', 'class') == '7')  {		// when Item Class is PCD Dies	or	PCD Die Recutting	
		nlapiDisableLineItemField('item', "custcol_pcd_grain", false);		
		setPCD_Fields();
	} else {
		nlapiDisableLineItemField('item', "custcol_pcd_grain", true);
	}
	setPCDHoleRange();	
}

function setPCD_Fields() {
	// PCD Grain List
	jQuery('input[name="inpt_custcol_pcd_grain"]').unbind('click');
 	jQuery('input[name="inpt_custcol_pcd_grain"]').on('click', function() {
 		console.log('Open PCD Grain List!'); 		
 		waitPCDGrainListDropDown();
 	});

 	// setPCDHoleRange();
}

function waitPCDGrainListDropDown() {
	if(jQuery('div.uir-tooltip-content div.dropdownDiv').length == 0) {		
		setTimeout(function() {waitPCDGrainListDropDown();}, 500);
	} else {
		console.log('PCD Grain List Opended!'); 
		setPCDGrainList();
	}
}

function toggleNanoSpecification_Fields() {
	console.log('--Enable/Disable NanoSpecification Fields--');
	var fieldValue = nlapiGetCurrentLineItemValue('item', 'class');	
	if(nlapiGetCurrentLineItemValue('item', 'class') == '1') {		// when Item Class is Nano Dies
		nlapiDisableLineItemField('item', "custcol_preferences", false);
		var factory_setting = 'Factory'
		if(nlapiGetCurrentLineItemValue('item', 'custcol_preferences') == '3') {	//Use Factory Standard			

			// nlapiSetCurrentLineItemValue('item', 'custcol_compacting_ratio', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_bearing_zone', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_drawing_angle', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_entry_angle', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_exit_angle', factory_setting);
		} else if(nlapiGetCurrentLineItemValue('item', 'custcol_preferences') == '2') {	 // Compacting ratio			

			// nlapiSetCurrentLineItemValue('item', 'custcol_bearing_zone', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_drawing_angle', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_entry_angle', factory_setting);
			// nlapiSetCurrentLineItemValue('item', 'custcol_exit_angle', factory_setting);
		} 		
	} 
}



function myFieldChanged(type, name, linenum) { 
	console.log ('--myFieldChanged--' );
	// console.log ('type=' + type);
	console.log ('name=' + name );
	// console.log ('linenum=' + linenum);	
	if(type == 'item' && name == 'item') {	// Add/Change Line Item
		initLineItem();
		autoSetLineItemFields();
	} else if(type == 'item' && name == 'class') {
		togglePCD_Fields();
		// toggleNanoSpecification_Fields();
	} else if(type == 'item' && name == 'custcol_preferences') {
		// toggleNanoSpecification_Fields();
	}


}


function myLineInit(type) {
	console.log ('--myLineInit--');
	console.log ('type=' + type);
	if(type == 'item') {
		initLineItem();	
		calcTotalOrderWeight();	
	}
}

function initLineItem() {
	console.log('--Init Line Item--');
	var item_id = nlapiGetCurrentLineItemValue('item', 'item');
	if(!isEmpty(item_id) && (!cur_item || cur_item['id'] != item_id)) {		
		cur_item = getLineItemRecord(item_id);
	}	
	togglePCD_Fields();
	// toggleNanoSpecification_Fields();
	setWireDiameterRange();	
}

function myValidateLine(type) {
	// itemtype: "InvtPart"
	console.log ('myValidateLine' );
	console.log ('type=' + type);
	var disabledValidation = nlapiGetFieldValue('custbody_disable_custom_validation');
	if(disabledValidation != 'T') {

		if(nlapiGetCurrentLineItemValue('item', 'itemtype') != 'InvtPart') return true;	// ignore validation
		if(nlapiGetCurrentLineItemValue('item', 'class') == '8') return true; 	// ignore validation when item class is 'Internal'
		if(nlapiGetCurrentLineItemValue('item', 'class') == '4') return true; 	// ignore validation when item class is 'ND/MD Dies'
		if(nlapiGetCurrentLineItemValue('item', 'class') == '6') return true; 	// ignore validation when item class is 'Shaped Wire Dies'
		if(nlapiGetCurrentLineItemValue('item', 'class') == '3') return true; 	// ignore validation when item class is 'TC Dies'		

		var required_fields = [
			{ id:"custcol_act_materal", label: "Actual Material"},
			{ id:"custcol_inc_wire_diameter", label: "Diameter"},
			{ id:"custcol_preferences", label: "Nano Specifications"},
			{ id:"custcol_case_height", label: "Case Diameter"},
			{ id:"custcol_case_diam", label: "Case Height"},
			{ id:"custcol_die_usage", label: "Die Usage"}
		];

		if(!validateDynamicFields(required_fields)) {
			return false;
		}


		if(nlapiGetCurrentLineItemValue('item', 'class') == '2' || nlapiGetCurrentLineItemValue('item', 'class') == '7')  {		// when Item Class is PCD Dies	or	PCD Die Recutting	
			var required_fields = [
				{ id:"custcol_pcd_grain", label: "PCD Grain"}
			];
			if(!validateDynamicFields(required_fields)) {
				return false;
			}
			
		} else if(nlapiGetCurrentLineItemValue('item', 'class') == '1') {		// when Item Class is Nano Dies
			if(nlapiGetCurrentLineItemValue('item', 'custcol_preferences') == '2') {	 // Compacting ratio
				var required_fields = [
					{ id:"custcol_compacting_ratio", label: "Compacting Ratio"}				
				];
				if(!validateDynamicFields(required_fields)) {
					return false;
				}
			} else if(nlapiGetCurrentLineItemValue('item', 'custcol_preferences') == '1') {	 // Zone, Angle fields to be selected
				var required_fields = [
					{ id:"custcol_compacting_ratio", label: "Compacting Ratio"},
					{ id:"custcol_bearing_zone", label: "Bearing Zone"},
					{ id:"custcol_drawing_angle", label: "Drawing Angle"},
					{ id:"custcol_entry_angle", label: "Entry Angle"},
					{ id:"custcol_exit_angle", label: "Exit Angle"}
				];
				if(!validateDynamicFields(required_fields)) {
					return false;
				}
			}
		}

		if(nlapiGetRecordType() == 'salesorder') {
			if(nlapiGetCurrentLineItemValue('item', 'class') == '1') {		// when Item Class is Nano Dies
				if(nlapiGetCurrentLineItemValue('item', 'custcol_die_usage') == '1') {		// when Die Usage is Drawing
					var required_fields = [
						{ id:"custcol_pcd_hole_size", label: ""}				
					];
					if(!validateDynamicFields(required_fields)) {
						return false;
					}
				}	
			}
		}
	}	
	return true;
}

function validateDynamicFields(arr_required_fields) {	
	var errorFields = [];		
	for(var i = 0; i < arr_required_fields.length; i++) {
		
		if(isEmpty(nlapiGetCurrentLineItemValue('item', arr_required_fields[i].id))) {
			var field = nlapiGetLineItemField('item', arr_required_fields[i].id, 1);			
			errorFields.push(field.getLabel());
			isValid = false;
		}
	}

	if(errorFields.length > 0) {
		alert('Please enter value(s) for: ' + errorFields.join(','));	
		return false;
	} 
	
	return true;
}



function autoSetLineItemFields() {
	// Set Standard Casing Size
	var casingSizeId = cur_item['custitem_std_casing_size'];
	var casingSizeName = casingSizeList[casingSizeId];
	if(casingSizeName) {
		nlapiSetCurrentLineItemValue('item', 'custcol_std_casing_size', casingSizeName);
	}	
	// Init Actual Material
	nlapiSetCurrentLineItemValue('item', 'custcol_act_materal','');
	// Init Incoming Wire Diamter
	nlapiSetCurrentLineItemValue('item', 'custcol_inc_wire_diameter','');
}


function calcTotalOrderWeight() {
	console.log('calcTotalOrderWeight');
	var total_weight = 0;
	for (var k = 1; k <= nlapiGetLineItemCount('item'); k++)
    {
    	// console.log('k', k);
    	var item_weight = 0;
    	var item_quantity = 0;
    	if(!isEmpty(nlapiGetLineItemValue('item','quantity', k))) {
    		item_quantity = parseFloat(nlapiGetLineItemValue('item','quantity', k));
    	}
      	var casing_height = nlapiGetLineItemValue('item','custcol_case_height', k);     
      	var casing_dia = nlapiGetLineItemValue('item','custcol_case_diam', k);     
      	// console.log('casing_height', casing_height);
      	// console.log('casing_dia', casing_dia);
      	if(!isEmpty(casing_height) && !isNaN(casing_height) && !isEmpty(casing_dia) && !isNaN(casing_dia)) {
      		item_weight = casing_height * 3.14 * Math.pow(casing_dia / 2, 2) * 0.00000805;
      		// console.log('item_weight', item_weight);

      	}
      	total_weight += item_weight * item_quantity;
    }

    nlapiSetFieldValue('custbody_total_order_weigh', total_weight.toFixed(2) + 'kg');
}


/**
 * Determines if a variable is either set to null or is undefined.
 *
 * @param {String}
 *             value - The value to test
 * @returns {Boolean} true if the variable is null or undefined, false if otherwise.
 */
function isNullOrUndefined(value) {
    if (value === null) {
        return true;
    }
    if (value === undefined) {
        return true;
    }
    return false;
}

/**
 * Determines if a string variable is empty or not. An empty string variable is one which is null or undefined or has a length of zero.
 *
 * @param {String}
 *             stValue - The string value to test for emptiness.
 * @throws {nlobjError}
 *              isEmpty should be passed a string value. The data type passed is {x} whose class name is {y}
 * @returns {Boolean} true if the variable is empty, false if otherwise.
 */
function isEmpty(stValue) {
    if (isNullOrUndefined(stValue) === true) {
        return true;
    }
    if (stValue.length == 0) {
        return true;
    }
    return false;
}


// 330898