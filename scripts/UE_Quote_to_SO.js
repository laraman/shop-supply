function afterSubmit(type,form)
{
	
	try
	{
		var currentContext = nlapiGetContext();
		nlapiLogExecution('DEBUG', 'context', currentContext.getExecutionContext());

		if (type != 'create' || currentContext.getExecutionContext() == 'userinterface') return;

		var custRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());		

		var refRecId = nlapiGetFieldValue('createdfrom');

		if(!refRecId) return;

		var refRecType = nlapiLookupField('transaction', refRecId, 'recordType');		
		nlapiLogExecution('DEBUG', 'refRecType', refRecType);

		if(refRecType != 'estimate') return;

		var refRec = nlapiLoadRecord('estimate', refRecId);
		nlapiLogExecution('DEBUG', 'refRec', JSON.stringify(refRec));

		var lineFieldsToAdd = [
			'custcol_act_materal',
			'custcol_inc_wire_diameter',
			'custcol_preferences',
			'custcol_case_height',
			'custcol_case_diam',
			'custcol_die_usage',
			'custcol_preferences',
			'custcol_bearing_zone',
			'custcol_compacting_ratio',
			'custcol_entry_angle',
			'custcol_exit_angle',
			'custcol_pcd_grain',
			'custcol_pcd_hole_size',
			'custcol_std_casing_size',
			'custcol_drawing_angle'
		];

		if(refRec) {
			for (var i = 1; i <= custRec.getLineItemCount('item'); i++) {			
				custRec.selectLineItem('item', i);				
				nlapiLogExecution('DEBUG', 'line num', i);
				for (var k = 0; k < lineFieldsToAdd.length; k++) {								
					var field_id = lineFieldsToAdd[k];
					var field_val = refRec.getLineItemValue('item', field_id, i);
					if(field_val) {
						nlapiLogExecution('DEBUG', field_id, field_val);
						custRec.setCurrentLineItemValue('item', field_id, field_val);	
					}
				}
				custRec.commitLineItem('item');
			}	

			nlapiSubmitRecord(custRec, true, true);
		}
		// createdfrom
	
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', e.getCode(), e.getDetails());
	}
}


function isNullOrUndefined(value) {
    if (value === null) {
        return true;
    }
    if (value === undefined) {
        return true;
    }
    return false;
}

function isEmpty(stValue) {
    if (isNullOrUndefined(stValue) === true) {
        return true;
    }
    if (stValue.length == 0) {
        return true;
    }
    return false;
}