function pageInit(type) {
 	console.log ('--myPageInit--' );
 	nlapiLogExecution('ERROR', '--myPageInit--', type);
 	console.log ('type=' + type); 
 	var supplier = nlapiGetFieldValue('entity');
 	console.log('supplier', supplier);

 	
	if(nda_dropship_suppliers.indexOf(supplier) != -1 && nlapiGetFieldValue('customform') != NDA_DROPSHIP_PO) {
		console.log('DROP SHIP');
		nlapiSetFieldValue('customform', NDA_DROPSHIP_PO);	// NDA Drop Ship Purchase Order Form
	}
	if(nda_suppliers.indexOf(supplier) != -1  && nlapiGetFieldValue('customform') != NDA_PO) {
		console.log('NORMAL');
		nlapiSetFieldValue('customform', NDA_PO);	
	}	
 	
 	
}

var NDA_DROPSHIP_PO = 114;
var NDA_PO = 113;

var nda_dropship_suppliers = [
		'5'					// SHANGHAI  JIAOYOU  DIAMOND  COATING  CO., LTD		
	,	'842'				// Beijing Sanxin Wire Drawing Die Co., Ltd
	];
var nda_suppliers = [
		'961'						// Shandong Dmbest Diamond Die Co., Ltd
	,	'840'						// Shanghai Willbe Electrico Equipment Ltd
	,	'837'						// Sichuan NTU Technology Co., Ltd.
	,	'844'						// YICHANG GEILI DIAMOND INDUSTRIAL CO.,LTD
	,	'952'						// Özel Plastik San ve Tic.Ltd.Şti.
	];	

function fieldChanged(type, name, linenum) { 	
	console.log ('name=' + name );
	
	
	if(name == 'entity') {
		console.log('SUPPLIER CHANGED');
		var supplier = nlapiGetFieldValue('entity');
		console.log('SUPPLIER', supplier);
						
		if(nda_dropship_suppliers.indexOf(supplier) != -1) {
			console.log('DROP SHIP');
			nlapiSetFieldValue('customform', NDA_DROPSHIP_PO);	// NDA Drop Ship Purchase Order Form
		}
		if(nda_suppliers.indexOf(supplier) != -1) {
			console.log('NORMAL');
			nlapiSetFieldValue('customform', NDA_PO);	
		}
	}	
}