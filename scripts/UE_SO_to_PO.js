var NDA_DROPSHIP_PO = 114;
var NDA_PO = 113;
var nda_dropship_suppliers = [
		'5'					// SHANGHAI  JIAOYOU  DIAMOND  COATING  CO., LTD		
	,	'842'				// Beijing Sanxin Wire Drawing Die Co., Ltd
	];
var nda_suppliers = [
		'961'						// Shandong Dmbest Diamond Die Co., Ltd
	,	'840'						// Shanghai Willbe Electrico Equipment Ltd
	,	'837'						// Sichuan NTU Technology Co., Ltd.
	,	'844'						// YICHANG GEILI DIAMOND INDUSTRIAL CO.,LTD
	,	'952'						// Özel Plastik San ve Tic.Ltd.Şti.
	];	

function afterSubmit(type,form)
{
	
	try
	{
		var currentContext = nlapiGetContext();
		nlapiLogExecution('DEBUG', 'context', currentContext.getExecutionContext());
		nlapiLogExecution('DEBUG', 'type', type);

		if (type != 'dropship' && type != 'create') return;

		// set default form id for PO
		var supplierId = nlapiGetFieldValue('entity');
		if(!supplierId) return;

		if(type == 'dropship') {
		 	if(nda_dropship_suppliers.indexOf(supplierId) != -1) {
				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'customform', NDA_DROPSHIP_PO);				
			} 
			if(nda_suppliers.indexOf(supplierId) != -1) {
				nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'customform', NDA_PO);				
			}
		}

		// get LeadTime for supplier 

		var leadtime = nlapiLookupField('vendor', supplierId, 'custentity_supplier_leadtime');
		if(!isEmpty(leadtime) && !isNaN(leadtime)) {
			var duedate = new Date();
			var dateValue = duedate.getDate() * 1 + parseInt(leadtime);
			duedate.setDate(dateValue);			
			nlapiSubmitField(nlapiGetRecordType(), nlapiGetRecordId(), 'duedate', nlapiDateToString(duedate));			
		}


		// if (type != 'dropship' && type != 'create') return;

		// var poRec = nlapiLoadRecord(nlapiGetRecordType(), nlapiGetRecordId());		

		// var refRecId = nlapiGetFieldValue('createdfrom');
		// nlapiLogExecution('DEBUG', 'refRecId', refRecId);

		// if(!refRecId) return;

		// var refRecType = nlapiLookupField('transaction', refRecId, 'recordType');		
		// nlapiLogExecution('DEBUG', 'refRecType', refRecType);

		// if(refRecType != 'salesorder') return;

		// var refRec = nlapiLoadRecord('salesorder', refRecId);
		// // nlapiLogExecution('DEBUG', 'refRec', JSON.stringify(refRec));

		// if(refRec) {
		// 	var field_val = refRec.getFieldValue('enddate');
		// 	nlapiLogExecution('DEBUG', 'Due Date', field_val);
		// 	if(field_val) {
		// 		poRec.setFieldValue('duedate', field_val);	
		// 	}

		// 	nlapiSubmitRecord(poRec, true, true);
		// }

	
	}
	catch (e)
	{
		nlapiLogExecution('ERROR', e.getCode(), e.getDetails());
	}
}


function isNullOrUndefined(value) {
    if (value === null) {
        return true;
    }
    if (value === undefined) {
        return true;
    }
    return false;
}

function isEmpty(stValue) {
    if (isNullOrUndefined(stValue) === true) {
        return true;
    }
    if (stValue.length == 0) {
        return true;
    }
    return false;
}