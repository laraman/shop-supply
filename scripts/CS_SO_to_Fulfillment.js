

function myPageInit(type) {
 	console.log ('--myPageInit--' );
 	console.log ('type=' + type); 
 	
}

function mySaveRecord() {
	var disabledValidation = nlapiGetFieldValue('custbody_disable_custom_validation');
	if(disabledValidation != 'T') {
		for (var k = 1; k <= nlapiGetLineItemCount('item'); k++)
	    {
	    	if(isEmpty(nlapiGetLineItemValue('item', 'custcol_custom_serial_num', k))) {
	    		alert('Serical Number is required. line -' + k);
	    		return false;		
	    	}
	    }
		
	}
	return true;
}





/**
 * Determines if a variable is either set to null or is undefined.
 *
 * @param {String}
 *             value - The value to test
 * @returns {Boolean} true if the variable is null or undefined, false if otherwise.
 */
function isNullOrUndefined(value) {
    if (value === null) {
        return true;
    }
    if (value === undefined) {
        return true;
    }
    return false;
}

/**
 * Determines if a string variable is empty or not. An empty string variable is one which is null or undefined or has a length of zero.
 *
 * @param {String}
 *             stValue - The string value to test for emptiness.
 * @throws {nlobjError}
 *              isEmpty should be passed a string value. The data type passed is {x} whose class name is {y}
 * @returns {Boolean} true if the variable is empty, false if otherwise.
 */
function isEmpty(stValue) {
    if (isNullOrUndefined(stValue) === true) {
        return true;
    }
    if (stValue.length == 0) {
        return true;
    }
    return false;
}
