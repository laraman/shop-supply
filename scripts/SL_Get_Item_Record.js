function onRequest(request, response)
{
	nlapiLogExecution('error', 'onRequest', '');
	var item_id = request.getParameter('item_id');

	var recordType = nlapiLookupField('item', item_id, 'recordType');          
    var rec = nlapiLoadRecord(recordType, item_id);

    var item = {};
    var fields = [
        'id', 
        'custitem9',
        'custitem_pcd_grain',
        'custitem_incoming_wire_dia_range',
        'custitem_pcd_hole_size',
        'custitem_std_casing_size'
     ];

    for(var i = 0; i < fields.length; i++) {
        var fieldId = fields[i];        
        var fieldVal = rec.getFieldValue(fieldId);                
        if(fieldId == 'custitem9' || fieldId == 'custitem_pcd_grain') { // Multi select 
            if(fieldVal) {
                fieldVal = fieldVal.split('\u0005');
            }
        } 
        item[fieldId] = fieldVal;
    }
    nlapiLogExecution('error', 'item', JSON.stringify(item));
    response.setContentType('JSON');
	response.write(JSON.stringify(item));		
}