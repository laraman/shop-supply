function onRequest(request, response)
{
	nlapiLogExecution('error', 'onRequest', '');
	var listid = request.getParameter('listid');

	var searchColumn = new nlobjSearchColumn('name');
    var searchResults = nlapiSearchRecord(listid, null, null, searchColumn);
    var listArray = new Array();
    for (i in searchResults) {    	    	
        listArray[searchResults[i].id] = searchResults[i].getValue(searchColumn);
    }

    response.setContentType('JSON');
	response.write(JSON.stringify(listArray));		
}