var phrases = [];
function adn_FieldChanged(type, name, linenum) { 
	console.log ('myFieldChanged' );
	console.log ('name=' + name );
	var phrase_id = nlapiGetFieldValue('custbody_add_phrase');
	if(name == 'custbody_add_phrase' && phrase_id != '') {
		if(!phrases || phrases.length > 0) phrases = getCustomList('customlist215');
		var phrase = phrases[phrase_id];
		if(phrase && phrase != '') {
			var txtValue = nlapiGetFieldValue('custbody_terms_and_conditions');
			txtValue += ' ' + phrase;
			nlapiSetFieldValue('custbody_terms_and_conditions', txtValue);
		}		

		setTimeout(function() { nlapiSetFieldValue('custbody_add_phrase', '');}, 2000);
	}
}


function getCustomList(listScriptId) {
	console.log('--getCustomList--', listScriptId);
	var url = nlapiResolveURL('SUITELET', 'customscript_bb_get_custom_list', 'customdeploy_bb_get_custom_list');
	var jsonData = {listid: listScriptId}
	var response = nlapiRequestURL(url, jsonData, null, 'POST');	
	var listArray = JSON.parse(response.getBody());    
    console.log('customList: ' + listScriptId, listArray);
    return listArray;
}